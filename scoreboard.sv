`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.05.2021 15:20:44
// Design Name: 
// Class Name: scoreboard
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

class scoreboard;

    virtual general_if v_io;
    
    function new (virtual general_if v_io);
        this.v_io = v_io;
    endfunction

     task compare();
     
        for(int i = 0; i < 32; i++) begin
            assert ( v_io.dut_registers[i] == v_io.model_registers[i])
                $display("Expected and Measured Results of Register[%0d] are equal.", i);
            else
                $error("Expected and Measured Results of Register[%0d] are not equal,\n",i,
                       "Expected_Result = %0h,\n", v_io.model_registers[i],
                       "Measured_Result = %0h\n", v_io.dut_registers[i]);
                       
        end
   
     endtask

endclass
