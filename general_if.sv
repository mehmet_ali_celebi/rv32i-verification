`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/10/2021 03:48:06 PM
// Design Name: 
// Module Name: general_if
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


interface general_if( input clk, rst );

    logic [31:0] instruction;
    logic [31:0] dut_registers [31:0];
    logic [31:0] model_registers [31:0];
    
endinterface
