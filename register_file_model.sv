`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.05.2021 16:05:04
// Design Name: 
// Class Name: register_file_model
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


class register_file_model;

    virtual general_if v_io;
    
    logic [31:0] register [31:0];
    
    function new (virtual general_if v_io);
        this.v_io = v_io;
    
    endfunction
    
    function write();
        for(int i = 0; i < 32; i++) begin
            v_io.model_registers[i] = register[i];
        end
    endfunction
    
    task run_phase();
        if(v_io.rst == 1'b1) begin
            for(int i = 0; i < 32; i++) begin
                register[i] = 32'h0000_0000;        
            end
        end
        else begin             
            case(v_io.instruction[6:0]) //Opcode
                7'h13/*I-Format Immediate*/:
                    begin
                        case(v_io.instruction[14:12])/*func3*/
                            3'h0/*ADDI*/:
                            begin
                                register[v_io.instruction[11:7]] = register[v_io.instruction[19:15]] + v_io.instruction[31:20];
                            end
                            3'h1/*SLLI*/:
                            begin
                                register[v_io.instruction[11:7]] = register[v_io.instruction[19:15]] << v_io.instruction[31:20];
                            end
                            3'h2/*SLTI*/:
                            begin
                                register[v_io.instruction[11:7]] = ($signed(register[ v_io.instruction[19:15]]) < $signed(v_io.instruction[31:20])) ? 1 : 0;
                            end
                            3'h3/*SLTIU*/:
                            begin
                                register[v_io.instruction[11:7]] = (register[ v_io.instruction[19:15]]< v_io.instruction[31:20]) ? 1 : 0;
                            end
                            3'h4/*XORI*/:
                            begin
                                register[v_io.instruction[11:7]] = register[v_io.instruction[19:15]] ^ v_io.instruction[31:20];
                            end
                            3'h5/*SRLI & SRAI*/:
                                begin
                                    case(v_io.instruction[30])
                                        1'b0/*SRLI*/:
                                        begin
                                            register[v_io.instruction[11:7]] = register[v_io.instruction[19:15]] >> v_io.instruction[31:20];
                                        end
                                        1'b1/*SRAI*/:
                                        begin
                                            register[v_io.instruction[11:7]] = register[v_io.instruction[19:15]] >>> v_io.instruction[31:20];
                                        end
                                    endcase
                                end/*END SRLI & SRAI*/
                            3'h6/*ORI*/:
                            begin
                                register[v_io.instruction[11:7]] = register[v_io.instruction[19:15]] | v_io.instruction[31:20];
                            end
                            3'h7/*ANDI*/:
                            begin
                                register[v_io.instruction[11:7]] = register[v_io.instruction[19:15]] & v_io.instruction[31:20];
                            end
                        endcase
                    end/*End I-Format Immediate*/                               
                7'h33/*R-Format*/: 
                    begin
                        case(v_io.instruction[14:12])/*func3*/
                            3'h0/*ADD & SUB*/:
                                begin
                                    case(v_io.instruction[30])/*func7*/
                                        1'h0/*ADD*/:
                                        begin
                                            register[v_io.instruction[11:7]] = register[v_io.instruction[19:15]] + register[v_io.instruction[24:20]];
                                        end
                                        1'h1/*SUB*/: 
                                        begin
                                            register[v_io.instruction[11:7]] = register[v_io.instruction[19:15]] - register[v_io.instruction[24:20]];
                                        end
                                        default: ;
                                    endcase /*End func7*/
                                end/*End ADD & SUB*/
                            3'h1/*SLL*/:
                            begin
                                register[v_io.instruction[11:7]] = register[v_io.instruction[19:15]] << register[v_io.instruction[24:20]];
                            end
                            3'h2/*SLT*/:
                            begin
                                register[v_io.instruction[11:7]] = ($signed(register[ v_io.instruction[19:15]]) < $signed(register[v_io.instruction[24:20]])) ? 1 : 0;
                            end
                            3'h3/*SLTU*/:
                            begin
                                register[v_io.instruction[11:7]] = (register[ v_io.instruction[19:15]] < register[v_io.instruction[24:20]]) ? 1 : 0;
                            end
                            3'h4/*XOR*/:
                            begin
                                register[v_io.instruction[11:7]] = register[v_io.instruction[19:15]] ^ register[v_io.instruction[24:20]];
                            end
                            3'h5/*SRL & SRA*/:
                                begin
                                    case(v_io.instruction[30])
                                        1'b0/*SRL*/:
                                        begin
                                            register[v_io.instruction[11:7]] = register[v_io.instruction[19:15]] >> register[v_io.instruction[24:20]];
                                        end
                                        1'b1/*SRA*/:
                                        begin
                                            register[v_io.instruction[11:7]] = register[v_io.instruction[19:15]] >>> register[v_io.instruction[24:20]];
                                        end
                                    endcase
                                end
                            3'h6/*OR*/:
                            begin
                                register[v_io.instruction[11:7]] = register[v_io.instruction[19:15]] | register[v_io.instruction[24:20]];
                            end
                            3'h7/*AND*/:
                            begin
                                register[v_io.instruction[11:7]] = register[v_io.instruction[19:15]] & register[v_io.instruction[24:20]];
                            end
                        endcase/*End func3*/
                    end/*End R-Type*/                   
                default: begin end
            endcase
        end
        register[0] = 32'h0000_0000;
        write();
	endtask
    
endclass
