`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/10/2021 03:47:03 PM
// Design Name: 
// Module Name: test
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module test( general_if io );
    import model_pkg::*;
    
    register_file_model reg_model;
    scoreboard scrbrd;
    
    initial begin
        reg_model = new(io);
        scrbrd = new(io);
    end
    
    always@(posedge io.clk) begin
        reg_model.run_phase;
        scrbrd.compare;
        if(io.instruction == 32'hAAAA_AAAA) 
            $finish();
    end
    
endmodule
