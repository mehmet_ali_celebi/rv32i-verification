`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/10/2021 03:44:00 PM
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top;

    bit clock;
    bit reset;
    
    general_if gen_if(.clk(clock), .rst(reset));
    
    `include "assignment.sv";
    assign gen_if.instruction = DUT.instruction;
    
    initial begin
        reset = 1'b1;
        #20;
        reset = 1'b0;
    end

    initial begin
        clock = 1'b0;
        forever begin #10 clock = ~clock; end
    end

    test test1(
        .io(gen_if)
    );
    
    rv32i DUT(
        .clk(clock),
        .rst(reset)
    );
    
endmodule
